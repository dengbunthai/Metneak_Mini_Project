package com.schoolmate.kshrd;


import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;


import com.schoolmate.kshrd.repository.GroupJpaRepository;
import com.schoolmate.kshrd.repository.NotificationJpaRepository;
import com.schoolmate.kshrd.repository.PostJpaRepository;
import com.schoolmate.kshrd.repository.RememberJpaRepository;
import com.schoolmate.kshrd.repository.SchoolJpaRepository;
import com.schoolmate.kshrd.repository.UserJpaRepository;
//import com.schoolmate.kshrd.repository.SchoolTestRepository;
//import com.schoolmate.kshrd.repository.UserSchoolTestRepository;
//import com.schoolmate.kshrd.repository.UserTestRepository;
import com.schoolmate.kshrd.service.PostService;
import com.schoolmate.kshrd.service.RememberService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MetneakMiniProjectApplicationTests {

	
	Logger log = LoggerFactory.getLogger(getClass());
	
//	@Autowired
//	UserSchoolTestRepository ustr;
//	
//	@Autowired
//	SchoolTestRepository str;
//	
//	@Autowired
//	UserTestRepository utr;
	
	@Autowired
	PostJpaRepository pjr;
	
	@Autowired
	PostService postService;
	
	@Autowired
	UserJpaRepository ujr;
	
	@Autowired
	GroupJpaRepository gjp;
	
	@Autowired
	SchoolJpaRepository sjr;
	
	@Autowired
	RememberJpaRepository rememberJpaRepository;
	
	@Autowired
	NotificationJpaRepository notificationJpaRepository;
	
	@Autowired
	RememberService rememberService;
	@Test
	public void contextLoads() {
		
		
		log.info("=> {}", rememberJpaRepository.findByUser_UserIdAndStatus(1, true, new PageRequest(0,1)));
		
		
		
//		RememberPrimaryKey rp = new RememberPrimaryKey();
//		rp.setUser_id(1);
//		rp.setMate_id(2);
//		Remember r = new Remember();
//		User u = new User();
//		u.setUserId(1);
//		User m = new User();
//		m.setUserId(2);
//		r.setUser(u);
//		r.setMate(m);
//	rememberJpaRepository.save(r);

//		rememberJpaRepository.userRememberMate();
		
//	User user = new User();
//	user.setFacebookId("m2");
//	
//	UserSchool us = new UserSchool();
//	us.getUserSchoolId().setSchool_id(1);
////	School s = sjr.findOne(1);
////	us.setSchool(s);
//	user.addUserSchools(us);
//	ujr.save(user);	
		
//		log.info("=> {}", ujr.findByFacebookId("f1"));
		
//====================== How to save composite entity ============
		
//		UserSchool us = new UserSchool();
////		UserSchoolPrimaryKey usp = new UserSchoolPrimaryKey();
////		usp.setUser_id(1);
////		usp.setSchool_id(1);
////		us.setId(usp);
//		us.setUser(utr.findOne(1));
//		us.setSchool(str.findOne(1));
//		us.setStatus(false);
//		
//		ustr.save(us);
		
//==================================================================		
		
//		log.info("=> {}", pjr.countByGroup_Id(1));
		
//		Post post = new Post();
//		post.setContent("Hello");
//		post.setCoverUrl("www.cover");
//		post.setPhotoUrl("www.photo");
//		Group group = new Group();
//		group.setId(1);
//		post.setGroup(group);
//		User user = new User();
//		user.setId(1);
//		post.setUser(user);
//		postService.savePost(post);
		
	}


	
	
}
