package com.schoolmate.kshrd.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.schoolmate.kshrd.model.UserGroup;
import com.schoolmate.kshrd.model.UserGroupPrimaryKey;

public interface UserGroupJpaRepository extends JpaRepository<UserGroup, UserGroupPrimaryKey>{
	
	@Transactional
	@Modifying
	@Query("delete from UserGroup ug where ug.userGroupId.user_id=?1")
	public void deleteByUserId(int userId);
}
