package com.schoolmate.kshrd.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.schoolmate.kshrd.model.UserSchool;

public interface UserSchoolJpaRepository extends JpaRepository<UserSchool, Integer>{

	@Transactional
	@Modifying
	@Query("delete from UserSchool us where us.userSchoolId.user_id=?1")
	public void deleteByUserId(int userId);
	
}
