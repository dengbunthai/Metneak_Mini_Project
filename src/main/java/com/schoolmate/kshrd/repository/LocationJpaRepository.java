package com.schoolmate.kshrd.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.schoolmate.kshrd.model.Location;

public interface LocationJpaRepository extends JpaRepository<Location, Integer>{

	@Query("select distinct l.provinceName from Location l")
	public Page<Location> findProvinceNameDistinct(Pageable page);
	public Page<Location> findByProvinceName(String provinceName, Pageable page);
}
