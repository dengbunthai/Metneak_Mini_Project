package com.schoolmate.kshrd.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.schoolmate.kshrd.model.User;

public interface UserJpaRepository extends JpaRepository<User, Integer>{
	
	public User findByFacebookId(String facebookId);
	
	@Query("select distinct u from User u inner join u.userSchools us where us.school.name = ?1 and u.status=?2")
	public Page<User> findByUserSchoolName(String name, boolean status, Pageable page);
	
	@Query("select r.mate from User u inner join u.remembers r where CONCAT(r.mate.lastname,r.mate.firstname) like %?2% and r.mate.status = ?3 and u.userId=?1")
//	@Query("select r.mate from User u inner join u.remembers r where CONCAT(r.mate.lastname,r.mate.firstname) like ?2% and r.mate.status = ?3 and u.userId=?1")
	public Page<User> findMateByMateName(int userId, String name, boolean status, Pageable page);

	@Query("select r.mate from User u inner join u.remembers r where u.userId = ?1")
	public Page<User> findByRemembers_User_UserId(int userId, Pageable page);
	
}
