package com.schoolmate.kshrd.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.schoolmate.kshrd.model.Notification;

public interface NotificationJpaRepository extends JpaRepository<Notification, Integer>{
	
	@Query("select count(n) from Notification n")
	public int countNotification();
	
	@Modifying
	@Transactional
	@Query("update Notification n set n.status=?3 where n.user.userId = ?1 and n.targetUser.userId = ?2 and type='R'")
	public void setStatus(int userId, int targetUserId, boolean status);
}
