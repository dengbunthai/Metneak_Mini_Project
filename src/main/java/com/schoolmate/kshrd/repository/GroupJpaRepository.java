package com.schoolmate.kshrd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.schoolmate.kshrd.model.Group;

public interface GroupJpaRepository extends JpaRepository<Group, Integer>{

	@Query(value="select checkgroup(:name, :level, :year)", nativeQuery=true)
	public boolean checkByNameLevelYear(@Param("name")String name, @Param("level")String level, @Param("year")int year);
	
	public Group findByName(String name);

}
