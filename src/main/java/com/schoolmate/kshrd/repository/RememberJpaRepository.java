package com.schoolmate.kshrd.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.schoolmate.kshrd.model.Remember;
import com.schoolmate.kshrd.model.RememberPrimaryKey;

public interface RememberJpaRepository extends JpaRepository<Remember, RememberPrimaryKey>{

	@Query("select r from Remember r where r.rememberId.user_id = ?1 and r.rememberId.mate_id = ?2")
	public Remember findByUserIdAndMateId(int userId, int mateId);
	
	@Modifying
	@Transactional
	@Query("update Remember r set r.status=?3 where r.rememberId.user_id = ?1 and r.rememberId.mate_id = ?2")
	public void setState(int userId, int mateId, boolean status);
	
	public Page<Remember> findByUser_UserIdAndStatus(int userId, boolean status, Pageable page);
	
}
