package com.schoolmate.kshrd.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.schoolmate.kshrd.model.School;

public interface SchoolJpaRepository extends JpaRepository<School, Integer>{
	
	public Page<School> findByLevel(String level, Pageable page);
	public Page<School> findSchoolDistinctByNameLike(String name, Pageable page);
	
	@Query("select distinct s.name from School s where s.name like %:schoolName%")
	public Page<String> searchSchoolByName(@Param("schoolName") String schoolName, Pageable page);
}
