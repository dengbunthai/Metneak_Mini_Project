package com.schoolmate.kshrd.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.schoolmate.kshrd.model.Post;

public interface PostJpaRepository extends JpaRepository<Post, Integer>{

	// savePost(Created, Updated) toGroup, toMate have already existed
	
	// deletePost has already existed

	
	
	public Page<Post> findByStatus(boolean status, Pageable page);

	public Page<Post> findByGroup_groupIdAndStatus(int id, Pageable page, boolean status);
	
//	@Query("select p from Post p")
	@Query("select p from Post p where p.group.groupId = ?1  and (p.createdDate between ?2 and ?3) and p.status = ?4")
	public Page<Post> findByGroup_groupIdAndStatus(int postId, Date sDate, Date eDate, boolean status,Pageable page);
	
//	@Query("select p from Post p where p.postId = ?1 and p.createdDate between ?2 and ?3 and p.status = ?4")
//	public Page<Post> findByGroup_groupIdAndStatus1(int postId, Date sDate, Date eDate, boolean status,Pageable page);
	
	public Page<Post> findByUser_userIdAndStatus(int id, Pageable page, boolean status);
	
	public Page<Post> findByMate_userIdAndStatus(int id, Pageable page, boolean status);
	
	public int countByUser_userIdAndStatus(int id, boolean status);
	public int countByMate_userIdAndStatus(int id, boolean status);
	public int countByGroup_groupIdAndStatus(int id, boolean status);
	public int countByGroup_groupIdAndCreatedDateBetweenAndStatus(int id, Date sDate, Date eDate, boolean status);
	
	@Transactional
	@Modifying
	@Query("update Post p set p.status = ?2 where p.postId = ?1")
	public void updateStatus(int id, boolean status);
}
