package com.schoolmate.kshrd.rest_controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.schoolmate.kshrd.model.UserSchool;
import com.schoolmate.kshrd.service.UserSchoolService;

@RestController
@RequestMapping("/api/v1/userschools")
public class UserSchoolRestController {

	@Autowired
	UserSchoolService userSchoolService;
	
	@PostMapping
	public UserSchool UserSchool(@RequestBody UserSchool userSchool){
		return userSchoolService.saveUserSchool(userSchool);
	}
	
}
