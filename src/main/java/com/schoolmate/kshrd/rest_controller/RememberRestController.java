package com.schoolmate.kshrd.rest_controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.schoolmate.kshrd.model.Remember;
import com.schoolmate.kshrd.service.RememberService;

@RestController
@RequestMapping("api/v1/remembers")
public class RememberRestController {

	@Autowired
	RememberService rememberService;
	
	@PostMapping
	public Remember remembers(@RequestBody Remember remember){
//		System.out.println(remember.getRememberId().getUser_id()+","+ remember.getRememberId().getMate_id()+","+remember.getStatus());
		 rememberService.userRememberMate(remember.getRememberId().getUser_id(), remember.getRememberId().getMate_id(), remember.getStatus());
		return null;
	}
	
	
	@GetMapping("/user/{userId}")
	public Page<Remember> findByUserId(
			@PathVariable("userId")int userId, 
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size)
	{
		Page<Remember> remembers = null;
		if(size == 0)
			remembers = rememberService.findByUser_UserIdAndStatus(userId, true, new PageRequest(0, Integer.MAX_VALUE));
		else
			remembers = rememberService.findByUser_UserIdAndStatus(userId, true, new PageRequest(page, size));
		
		return remembers;
		
	}
	
}
