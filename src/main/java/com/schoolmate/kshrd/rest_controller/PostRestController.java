package com.schoolmate.kshrd.rest_controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;

import org.hibernate.cache.spi.TimestampsRegion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.schoolmate.kshrd.model.Post;
import com.schoolmate.kshrd.repository.PostJpaRepository;
import com.schoolmate.kshrd.service.PostService;

@RestController
@RequestMapping("api/v1/posts")
public class PostRestController {

	@Autowired
	PostService postService;
	
	@Autowired
	PostJpaRepository postJpaRepository;
	
	@PostMapping
	public Post savePost(@RequestBody Post post){
		return postService.savePost(post);
	}
	
	@PutMapping
	public Post updatePost(@RequestBody Post post){
		return postService.savePost(post);
	}	
	
	@DeleteMapping("{postId}")
	public Post deletePost(@PathVariable("postId")int id){
		return postService.deleteById(id);
	}
	
	@GetMapping
	public Page<Post> posts(
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size)
	{
		Page<Post> posts = null;
		
		if(size == 0)
			posts = postService.findAllPosts(new PageRequest(0, Integer.MAX_VALUE));
		else
			posts = postService.findAllPosts(new PageRequest(page, size));
		
		hateToas(posts);
		
		return posts;
	}
	
	@GetMapping("group/{groupId}")
	@Column(name = "find-by-group-locationId")
	public Page<Post> findByGroupId(
			@PathVariable("groupId") int id,
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size)
	{
		if(size == 0)
			return postService.findByGroupId(id,new PageRequest(0, Integer.MAX_VALUE));
		else
			return postService.findByGroupId(id,new PageRequest(page, size));
	}	
	
	@GetMapping("group/{groupId}/created-date")
	@Column(name = "find-by-group-locationId-and-created-date")
	public Page<Post> findByGroupIdAndCreatedDateBetween(
			@PathVariable("groupId") int id,
			@RequestParam("sDate") String sDate,
			@RequestParam("eDate") String eDate,
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size)
	{
		
		Date sD = new Timestamp(Long.parseLong(sDate));
		Date eD = new Timestamp(Long.parseLong(eDate));
		
		System.out.println("=> " + sD + ", " + eD);
		
		Page<Post> posts = null;
		
		if(size == 0)
			posts = postService.findByGroupIdAndCreatedDateBetween(id, sD, eD, new PageRequest(0, Integer.MAX_VALUE));
		else
			posts = postService.findByGroupIdAndCreatedDateBetween(id, sD, eD, new PageRequest(page, size));
		
		hateToas(posts);
		
		return posts;
	}		
	
	@GetMapping("user/{userId}")
	@Column(name = "find-by-user-locationId")
	public Page<Post> findByUserId(
			@PathVariable("userId") int userId,
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size)
	{
		
		Page<Post> posts = null;
		
		if(size == 0)
			posts = postService.findByUserId(userId,new PageRequest(0, Integer.MAX_VALUE));
		else
			posts = postService.findByUserId(userId,new PageRequest(page, size));
		
		hateToas(posts);
		
		return posts;
	}	
	
	
	@GetMapping("mate/{mateId}")
	@Column(name = "find-by-mate-locationId")
	public Page<Post> findByMateId(
			@PathVariable("mateId") int mateId,
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size)
	{
		Page<Post> posts = null;
		if(size == 0)
			posts = postService.findByMateId(mateId,new PageRequest(0, Integer.MAX_VALUE));
		else
			posts = postService.findByMateId(mateId,new PageRequest(page, size));
		
		
		hateToas(posts);		
		
		return posts;
	}

	private void hateToas(Page<Post> posts) {
		for(Post p : posts.getContent()){
			p.add(linkTo(methodOn(UserRestController.class).user(p.getUser().getUserId())).withRel("From_User"));
			if(p.getMate() != null){
				p.add(linkTo(methodOn(UserRestController.class).user(p.getMate().getUserId())).withRel("To_Mate"));
			}
			else
				p.add(linkTo(methodOn(GroupRestController.class).group(p.getGroup().getGroupId())).withRel("To_Group"));
		}
	}
	
	
	@GetMapping("user/count/{userId}")
	@Column(name = "count-by-user-locationId")
	public int countByUserId(@RequestParam("userId") int userId){
		return postService.countByUserId(userId);
	}
	
	@GetMapping("mate/count/{mateId}")
	@Column(name = "count-by-mate-locationId")
	public int countByMateId(@RequestParam("mateId") int mateId){
		return postService.countByMateId(mateId);
	}	
	
	@GetMapping("group/count/{groupId}")
	@Column(name = "count-by-group-locationId")
	public int countByGroupId(@RequestParam("groupId") int groupId){
		return postService.countByGroupId(groupId);
	}	
	
	@GetMapping("group/count/{groupId}/created-date")
	@Column(name = "find-by-group-locationId-and-created-date")
	public int countByGroupId(@RequestParam("groupId") int groupId, 
			@RequestParam("sDate") String sDate,
			@RequestParam("eDate") String eDate){
		
		Date sD = new Timestamp(Long.parseLong(sDate));
		Date eD = new Timestamp(Long.parseLong(eDate));
		
		return postService.countByGroupIdAndCreatedDateBetween(groupId, sD, eD);
	}	
	
}
