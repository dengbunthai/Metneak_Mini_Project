package com.schoolmate.kshrd.rest_controller;

import javax.persistence.Column;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.schoolmate.kshrd.model.Post;
import com.schoolmate.kshrd.model.School;
import com.schoolmate.kshrd.service.SchoolService;

@RestController
@RequestMapping("api/v1/schools")
public class SchoolRestController {

	@Autowired
	SchoolService schoolService;
	
	Sort mySort(String sort, String field){
		if(sort.equals("asc"))
			return new Sort(Direction.ASC, field);
		else if(sort.equals("desc"))
			return new Sort(Direction.DESC, field);
		
		return null;
	}
	
	@GetMapping("level/{level}")
	@Column(name="find-by-level")
	public Page<School> findByLevel(@PathVariable("level")String level,
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size,
			@RequestParam(name = "sort", defaultValue = "none") String sort,
			@RequestParam(name = "field", defaultValue = "name") String field
			) {
		
		Page<School> schools = null;
		
		Sort s = mySort(sort, field);
		
		if(size == 0)
			schools = schoolService.findByLevel(level,new PageRequest(0, Integer.MAX_VALUE, s));
		else
			schools = schoolService.findByLevel(level, new PageRequest(page, size, s));
		
		
		return schools;		
		
	}

	@GetMapping("name/{name}")
	@Column(name="find-by-name-like")
	public Page<School> findByNameLike(@PathVariable("name")String name,
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size) {
		
		Page<School> schools = null;
		
		if(size == 0)
			schools = schoolService.findPostDistinctByNameLike(name,new PageRequest(0, Integer.MAX_VALUE));
		else
			schools = schoolService.findPostDistinctByNameLike(name, new PageRequest(page, size));
		
		
		return schools;	
	}
}
