package com.schoolmate.kshrd.rest_controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.schoolmate.kshrd.model.Notification;
import com.schoolmate.kshrd.service.NotificationService;

@RestController
@RequestMapping("api/v1/notifications")
public class NotificationRestController {
	
	@Autowired
	NotificationService notificationService;
	
	@PostMapping
	public Notification notification(@RequestBody Notification notification){
		return notificationService.saveNotification(notification);
	}
	
	@GetMapping("/count")
	public int countNotification(){
		return notificationService.countNotification();
	}
}

