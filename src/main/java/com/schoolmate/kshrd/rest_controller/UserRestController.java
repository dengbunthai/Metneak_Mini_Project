package com.schoolmate.kshrd.rest_controller;

import javax.persistence.Column;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.schoolmate.kshrd.model.User;
import com.schoolmate.kshrd.model.UserSchool;
import com.schoolmate.kshrd.repository.UserJpaRepository;
import com.schoolmate.kshrd.service.UserService;


@RestController
@RequestMapping("api/v1/users")
public class UserRestController {

	@Autowired
	UserService userService;
	
	@Autowired
	UserJpaRepository userJpaRepository;
	
	@GetMapping
	public Page<User> users(			
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size)
	{
		Page<User> users = null;	
		
		if(size == 0)
			users = userService.findAllUser(new PageRequest(0, Integer.MAX_VALUE));
		else
			users = userService.findAllUser(new PageRequest(page, size));			
		
		hateOAS(users);
		return users;
	}
	
//	@GetMapping("/name/{userName}")
//	public Page<User> users(	
//			@PathVariable("userName") String name,
//			@RequestParam(name ="page", defaultValue = "0") int page, 
//			@RequestParam(name = "size", defaultValue = "0") int size)
//	{
//		Page<User> users = null;	
//		
//		if(size == 0)
//			users = userService.findAllUser(new PageRequest(0, Integer.MAX_VALUE)).get;
//		else
//			users = userService.findAllUser(new PageRequest(page, size));			
//		
//		hateOAS(users);
//		return users;
//	}	
//	
	@GetMapping("/{id}")
	public User user(@PathVariable("id") int id){
		 User u = userJpaRepository.findOne(id);
		 
			oneHateOas(u);
			
		return u;
	}

	
//	@GetMapping("/{userId}/mates")
//	public Page<User> mates(@PathVariable("userId") int userId,
//			@RequestParam(name ="page", defaultValue = "0") int page, 
//			@RequestParam(name = "size", defaultValue = "0") int size)
//	{
//		Page<User> users = null;	
//		
//		if(size == 0)
//			users = userService.findUserMate(userId,new PageRequest(0, Integer.MAX_VALUE));
//		else
//			users = userService.findUserMate(userId, new PageRequest(page, size));			
//		
//		hateOAS(users);
//		return users;
//	}
	
	private void oneHateOas(User u) {
		if(u.getRemembers() != null){
			u.add(linkTo(methodOn(RememberRestController.class).findByUserId(u.getUserId(), 0, 0)).withRel("Remember-Friends"));
		}
		if(u.getPosts() != null){
			u.add(linkTo(methodOn(PostRestController.class).findByUserId(u.getUserId(), 0, 0)).withRel("Posts"));
		}
	}
	
	@GetMapping("/facebook/{facebookId}")
	@Column(name="find-by-facebook-locationId")
	public User findByfacebookId(@PathVariable("facebookId")String facebookId){
		User u = userService.findByfacebookId(facebookId);
		oneHateOas(u);
		return u;
	}
	
	@GetMapping("/schoolname/{schoolName}")
	@Column(name="find-by-schoolname")
	public Page<User> findBySchoolName(
			@PathVariable("schoolName")String schoolName, 
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size){
//				System.out.println("=> "+schoolName);1
			
				Page<User> users = null;
		
				if(size == 0)
					users = userService.findByUserSchoolNameAndStatus(schoolName, true, new PageRequest(0, Integer.MAX_VALUE));
				else
					users = userService.findByUserSchoolNameAndStatus(schoolName, true, new PageRequest(page, size));
					
		
				hateOAS(users);
				
				return users;
	}

	private void hateOAS(Page<User> users) {
		for(User u : users.getContent()){
			
			oneHateOas(u);
			
		}
	}
	
	
	@GetMapping("/{userId}/mates/name")
	@Column(name="find-by-matename")
	public Page<User> findMateByMateName(
			@PathVariable("userId")int userId,
			@RequestParam("mateName")String mateName, 
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size){
		
		
			Page<User> users = null;
				if(size == 0)
					users = userService.findMateByMateName(userId, mateName,new PageRequest(0, Integer.MAX_VALUE));
				else
					users = userService.findMateByMateName(userId, mateName, new PageRequest(page, size));
			
			hateOAS(users);
			
			return users;
		
	}	
	
	@PostMapping
	@Column(name="user")
	public User saveUser(@RequestBody User user){
		return userService.saveUser(user);
	}
	
	@PutMapping
	@Column(name="user")
	public User updateUser(@RequestBody User user){
		return userService.saveUser(user);
	}
}
