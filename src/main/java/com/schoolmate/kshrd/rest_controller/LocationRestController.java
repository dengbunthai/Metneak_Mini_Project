package com.schoolmate.kshrd.rest_controller;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.schoolmate.kshrd.model.Location;
import com.schoolmate.kshrd.service.LocationService;

@RestController
@RequestMapping("/api/v1/locations")
public class LocationRestController {

	@Autowired
	LocationService locationService;
	
	@GetMapping("/distinct-by-province-name")
	@Column(name="distinct-by-province-name")
	public Page<Location> findProvinceNameDistinct(
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size){

		Page<Location> locations = null;
		if(size == 0)
			locations = locationService.findProvinceNameDistinct(new PageRequest(0, Integer.MAX_VALUE));
		else
			locations = locationService.findProvinceNameDistinct(new PageRequest(page, size));
		
		return locations;
	}
	
	@GetMapping("/find-by-province-name/{provinceName}")
	@Column(name="find-by-province-name")
	public Page<Location> findByProvinceName(@PathVariable("provinceName") String provinceName,
			@RequestParam(name ="page", defaultValue = "0") int page, 
			@RequestParam(name = "size", defaultValue = "0") int size){
	
			Page<Location> locations = null;
			if(size == 0)
				locations = locationService.findByProvinceName(provinceName, new PageRequest(0, Integer.MAX_VALUE));
			else
				locations = locationService.findByProvinceName(provinceName,new PageRequest(page, size));
			
			return locations;	
		
	}
	
}
