package com.schoolmate.kshrd.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mn_user")
@EntityListeners(AuditingEntityListener.class)
public class User extends ResourceSupport{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userId;
	private String firstname;
	private String lastname;
	private String username;
	private String gender;
	private Date dob;
	private String email;
	private String facebookId;
	
	@ManyToOne
	private Location location;
	
	@OneToMany(mappedBy="user")
	private List<UserSchool> userSchools = new ArrayList<>();
	
	@OneToMany(mappedBy="user")
	private List<Remember> remembers = new ArrayList<>();
	
	@OneToMany(mappedBy="mate")
	private List<Remember> rememberedBy = new ArrayList<>();
	
	@OneToMany(mappedBy="user")
	private List<UserGroup> userGroups = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="user")
	private List<Post> posts = new ArrayList<>();

//	@OneToMany(mappedBy="mate")
//	private List<Post> post = new ArrayList<>();	
	
	@OneToMany(mappedBy="user")
	private List<ReadNotification> readNotifications = new ArrayList<>();

	
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;
	
	private String description;
	private String role;
	private String profileUrl;
	private String coverUrl;
	private boolean status = true;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFacebookId() {
		return facebookId;
	}
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	public List<UserGroup> getUserGroups() {
		return userGroups;
	}
	public void setUserGroups(List<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}
	public List<Post> getPosts() {
		return posts;
	}
	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
	public List<ReadNotification> getReadNotifications() {
		return readNotifications;
	}
	public void setReadNotifications(List<ReadNotification> readNotifications) {
		this.readNotifications = readNotifications;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getProfileUrl() {
		return profileUrl;
	}
	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
	public String getCoverUrl() {
		return coverUrl;
	}
	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	public List<UserSchool> getUserSchools() {
		return userSchools;
	}
	

	
	public void setUserSchools(List<UserSchool> userSchools) {
		this.userSchools = userSchools;
	}
	public void addUserSchools(UserSchool userSchool) {
		if(userSchool != null){
			this.userSchools.add(userSchool);
			userSchool.setUser(this);
		}
	}	
	
	public List<Remember> getRemembers() {
		return remembers;
	}
	public void setRemembers(List<Remember> remembers) {
		this.remembers = remembers;
	}
	public List<Remember> getRememberedBy() {
		return rememberedBy;
	}
	public void setRememberedBy(List<Remember> rememberedBy) {
		this.rememberedBy = rememberedBy;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + "\\n firstname=" + firstname + "\\n lastname=" + lastname + "\\n username=" + username
				+ "\\n gender=" + gender + "\\n dob=" + dob + "\\n location=" + location + "\\n createdDate="
				+ createdDate + "\\n lastModifiedDate=" + lastModifiedDate + "\\n description=" + description
				+ "\\n role=" + role + "\\n profileUrl=" + profileUrl + "\\n coverUrl=" + coverUrl + "\\n status="
				+ status + "]";
	}
	
}
