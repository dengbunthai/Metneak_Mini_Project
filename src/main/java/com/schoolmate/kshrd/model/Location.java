package com.schoolmate.kshrd.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "mn_location")
public class Location {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int locationId;
	
	String provinceName;
	String districtName;
	
	boolean status = true;
	
	@OneToMany(mappedBy="location")
	List<User> users = new ArrayList<>();
	
	public Location() {
		super();
	}

	public Location(String provinceName, String districtName, boolean status) {
		super();
		this.provinceName = provinceName;
		this.districtName = districtName;
		this.status = status;
	}

	public Location(String provinceName, String districtName) {
		super();
		this.provinceName = provinceName;
		this.districtName = districtName;
	}

	public Location(int id, String provinceName, String districtName, boolean status) {
		super();
		this.locationId = id;
		this.provinceName = provinceName;
		this.districtName = districtName;
		this.status = status;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int id) {
		this.locationId = id;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
		
	
	
}
