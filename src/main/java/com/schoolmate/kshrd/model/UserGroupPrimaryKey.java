package com.schoolmate.kshrd.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserGroupPrimaryKey implements Serializable{

//	@Column(name = "user_id")
	private int user_id;
	
//	@Column(name = "group_id")
	private int group_id;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getGroup_id() {
		return group_id;
	}

	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
}
