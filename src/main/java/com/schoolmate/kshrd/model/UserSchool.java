package com.schoolmate.kshrd.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name="mn_user_school")
public class UserSchool {
	
	@EmbeddedId
	private UserSchoolPrimaryKey userSchoolId = new UserSchoolPrimaryKey();
	
	@ManyToOne
	@MapsId("user_id")
	@JoinColumn(name = "userId")
	@JsonIgnore
	private User user = new User();
	
	@ManyToOne
	@MapsId("school_id")
	@JoinColumn(name = "schoolId")
	private School school = new School();
	
	int startedYear;
	int graduatedYear;
	
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	Date createdDate;
	
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	Date lastModifiedDate;
	
	boolean status = true;

	public UserSchoolPrimaryKey getUserSchoolId() {
		return userSchoolId;
	}

	public void setUserSchoolId(UserSchoolPrimaryKey id) {
		this.userSchoolId = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public int getStartedYear() {
		return startedYear;
	}

	public void setStartedYear(int startedYear) {
		this.startedYear = startedYear;
	}

	public int getGraduatedYear() {
		return graduatedYear;
	}

	public void setGraduatedYear(int graduatedYear) {
		this.graduatedYear = graduatedYear;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "UserSchool [userSchoolId=" + userSchoolId.getUser_id()+"_"+userSchoolId.getSchool_id() + "\\n user=" + user.getUserId() + "\\n school=" + school.getSchoolId()
				+ "\\n startedYear=" + startedYear + "\\n graduatedYear=" + graduatedYear + "\\n createdDate="
				+ createdDate + "\\n lastModifiedDate=" + lastModifiedDate + "\\n status=" + status + "]";
	}
	
}


//@Entity
//@Table(name="mn_user_school")
//@IdClass(UserSchoolPrimaryKey.class)
//public class UserSchool {
//	
//	@Id
//	private int user_id;
//	@Id
//	private int school_id;
//	
//	
//	int startedYear;
//	int graduatedYear;
//	
//	@CreatedDate
//	Date createdDate;
//	
//	@LastModifiedDate
//	Date lastModifiedDate;
//	
//	boolean status = true;
//	
//}
