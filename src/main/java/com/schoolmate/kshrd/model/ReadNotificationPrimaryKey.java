package com.schoolmate.kshrd.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReadNotificationPrimaryKey implements Serializable{

//	@Column(name = "user_id")
	private int user_id;
	
//	@Column(name = "notification_id")
	private int notification_id;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getNotification_id() {
		return notification_id;
	}

	public void setNotification_id(int notification_id) {
		this.notification_id = notification_id;
	}
	
}
