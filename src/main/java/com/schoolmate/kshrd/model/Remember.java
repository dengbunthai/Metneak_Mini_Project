package com.schoolmate.kshrd.model;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mn_remember")

public class Remember {
	
	@EmbeddedId
	private RememberPrimaryKey rememberId = new RememberPrimaryKey();
	
	@ManyToOne
	@MapsId("user_id")
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;
	
	@ManyToOne
	@MapsId("mate_id")
	@JoinColumn(name = "mate_id")
	@JsonIgnore
	private User mate;
	
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private  Date rememberedDate;
	
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;
	
	private boolean status = true;

	public RememberPrimaryKey getRememberId() {
		return rememberId;
	}

	public void setRememberId(RememberPrimaryKey id) {
		this.rememberId = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getMate() {
		return mate;
	}

	public void setMate(User mate) {
		this.mate = mate;
	}

	public Date getRememberedDate() {
		return rememberedDate;
	}

	public void setRememberedDate(Date rememeredDate) {
		this.rememberedDate = rememeredDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
