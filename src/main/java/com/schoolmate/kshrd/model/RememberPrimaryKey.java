package com.schoolmate.kshrd.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RememberPrimaryKey implements Serializable{
	
//	@Column(name = "user_id")
	private int user_id;
//	@Column(name = "mate_id")
	private int mate_id;
	
	public int getUser_id() {
		return user_id;
	}
	
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	public int getMate_id() {
		return mate_id;
	}
	
	public void setMate_id(int mate_id) {
		this.mate_id = mate_id;
	}
	
}
