package com.schoolmate.kshrd.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mn_post")
@EntityListeners(AuditingEntityListener.class)
public class Post extends ResourceSupport{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer postId;
	
	@ManyToOne
//	@JsonIgnore
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne
//	@JsonIgnore
	@JoinColumn(name = "group_id")
	private Group group;
	
	@ManyToOne
//	@JsonIgnore
	@JoinColumn(name = "mate_id")
	private User mate;
	
	private String content;
	private String photoUrl;
	private String coverUrl;
	

	public Post() {
		super();
	}

	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;
	
	private boolean status = true;

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer id) {
		this.postId = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public User getMate() {
		return mate;
	}

	public void setMate(User mate) {
		this.mate = mate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCoverUrl() {
		return coverUrl;
	}

	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	@Override
	public String toString() {
		return "Post [postId=" + postId + "\\n content=" + content + "\\n photoUrl=" + photoUrl + "\\n coverUrl=" + coverUrl
				+ "\\n createdDate=" + createdDate + "\\n lastModifiedDate=" + lastModifiedDate + "\\n status=" + status
				+ "]";
	}
	
}