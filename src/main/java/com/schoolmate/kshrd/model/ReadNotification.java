package com.schoolmate.kshrd.model;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name="mn_read_notification")
public class ReadNotification {

	@EmbeddedId
	private ReadNotificationPrimaryKey readNotificationId = new ReadNotificationPrimaryKey();
	
	@ManyToOne
	@MapsId("user_id")
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne
	@MapsId("notification_id")
	@JoinColumn(name = "notification_id")
	private Notification notification;
	
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	public ReadNotificationPrimaryKey getReadNotificationId() {
		return readNotificationId;
	}

	public void setReadNotificationId(ReadNotificationPrimaryKey id) {
		this.readNotificationId = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
