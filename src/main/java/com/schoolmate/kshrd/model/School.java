package com.schoolmate.kshrd.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="mn_school")
public class School {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int schoolId;
	private String name;
	@Column(length=1)
	private String level;
	private boolean status;
	
	@OneToMany(mappedBy="school", orphanRemoval=true, fetch=FetchType.EAGER)
	@JsonIgnore
	private List<UserSchool> userSchools = new ArrayList<>();

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int id) {
		this.schoolId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<UserSchool> getUserSchools() {
		return userSchools;
	}

	public void setUserSchools(List<UserSchool> userSchools) {
		this.userSchools = userSchools;
	}
	
	
}
