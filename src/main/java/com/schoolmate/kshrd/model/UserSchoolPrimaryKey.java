package com.schoolmate.kshrd.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class UserSchoolPrimaryKey implements Serializable{
//	private static final long serialVersionUID = 1L;
//	@Column(name="user_id")
	private int user_id;
	
//	@Column(name="school_id")
	private int school_id;
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getSchool_id() {
		return school_id;
	}
	public void setSchool_id(int school_id) {
		this.school_id = school_id;
	}
	
	
}
