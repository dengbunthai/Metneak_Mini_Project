package com.schoolmate.kshrd.service;

import com.schoolmate.kshrd.model.Notification;

public interface NotificationService {
	public int countNotification();
	public Notification saveNotification(Notification notification);
}
