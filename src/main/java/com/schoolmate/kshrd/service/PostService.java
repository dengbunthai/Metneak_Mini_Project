package com.schoolmate.kshrd.service;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.schoolmate.kshrd.model.Post;

public interface PostService {
	
	public Post savePost(Post post);
	public Post deleteById(int id);
	public Page<Post> findAllPosts(Pageable page);
	public Page<Post> findByGroupId(int id, Pageable page);
	public Page<Post> findByGroupIdAndCreatedDateBetween(int id, Date sDate, Date eDate, Pageable page);
	public Page<Post> findByUserId(int id, Pageable page);
	public Page<Post> findByMateId(int id, Pageable page);
	public int countByUserId(int id);
	public int countByMateId(int id);
	public int countByGroupId(int id);
	public int countByGroupIdAndCreatedDateBetween(int id, Date sDate, Date eDate);
	
}
