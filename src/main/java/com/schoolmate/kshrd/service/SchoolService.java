package com.schoolmate.kshrd.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import com.schoolmate.kshrd.model.School;

public interface SchoolService {
	
	public Page<School> findByLevel(String level, Pageable page);
	public Page<School> findPostDistinctByNameLike(String name, Pageable page);
	public Page<String> searchSchoolByName(@Param("schoolName") String schoolName, Pageable page);
}
