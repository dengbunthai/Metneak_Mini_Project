package com.schoolmate.kshrd.service;

import com.schoolmate.kshrd.model.Group;

public interface GroupService {
	public Group findGroupById(int id);
}
