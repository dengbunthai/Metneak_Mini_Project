package com.schoolmate.kshrd.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.schoolmate.kshrd.model.Remember;

public interface RememberService {
	public Remember userRememberMate(int userId, int mateId, boolean status);
	public Page<Remember> findByUser_UserIdAndStatus(int userId, boolean status, Pageable page);
}
