package com.schoolmate.kshrd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.Notification;
import com.schoolmate.kshrd.model.Remember;
import com.schoolmate.kshrd.model.User;
import com.schoolmate.kshrd.repository.NotificationJpaRepository;
import com.schoolmate.kshrd.repository.RememberJpaRepository;
import com.schoolmate.kshrd.service.RememberService;

@Service
public class RememberServiceImpl implements RememberService{

	@Autowired
	RememberJpaRepository rememberJpaRepository;
	
	@Autowired
	NotificationJpaRepository notificationJpaRepository;
	
	@Override
	public Remember userRememberMate(int userId, int targetUserId, boolean status) {
//		rememberJpaRepository.userRememberMate(userId, mateId, true);
		
		if(rememberJpaRepository.findByUserIdAndMateId(userId, targetUserId) != null){
			rememberJpaRepository.setState(userId, targetUserId, status);
			notificationJpaRepository.setStatus(userId, targetUserId, status);
		}
		else{
			User u = new User();
			u.setUserId(userId);
			User m = new User();
			m.setUserId(targetUserId);
			
			Remember r = new Remember();
			r.setUser(u);
			r.setMate(m);
			rememberJpaRepository.save(r);
			
			Notification n = new Notification();
			n.setUser(u);
			n.setTargetUser(m);
			n.setType("R");
			notificationJpaRepository.save(n);
		}
		
		return null;
	}

	@Override
	public Page<Remember> findByUser_UserIdAndStatus(int userId, boolean status, Pageable page) {
		return rememberJpaRepository.findByUser_UserIdAndStatus(userId, status, page);
	}

}
