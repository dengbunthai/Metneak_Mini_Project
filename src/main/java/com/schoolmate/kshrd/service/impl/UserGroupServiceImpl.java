package com.schoolmate.kshrd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.repository.UserGroupJpaRepository;
import com.schoolmate.kshrd.service.UserGroupService;

@Service
public class UserGroupServiceImpl implements UserGroupService{

	@Autowired
	UserGroupJpaRepository userGroupJpaRepository;
	
	@Override
	public void deleteByUserId(int userId) {
		userGroupJpaRepository.deleteByUserId(userId);
	}

}
