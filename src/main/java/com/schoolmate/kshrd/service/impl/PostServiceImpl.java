package com.schoolmate.kshrd.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.Post;
import com.schoolmate.kshrd.repository.PostJpaRepository;
import com.schoolmate.kshrd.service.PostService;

@Service
public class PostServiceImpl implements PostService{

	
	@Autowired
	PostJpaRepository postJpaRepository;
	
	@Override
	public Post savePost(Post post) {
		
		System.out.println("Here: "+ post.getPostId());
		if(post.getPostId() != null){
			Post p = postJpaRepository.findOne(post.getPostId());
			p.setContent(post.getContent());
			p.setCoverUrl(post.getCoverUrl());
			p.setPhotoUrl(post.getPhotoUrl());
			postJpaRepository.save(p);
			return p;
		}
		else{
			postJpaRepository.save(post);
			return post;
		}
		
	}

	@Override
	public Page<Post> findByGroupId(int id, Pageable page) {
		// TODO Auto-generated method stub
		return postJpaRepository.findByGroup_groupIdAndStatus(id, page, true);
	}

	@Override
	public Page<Post> findByGroupIdAndCreatedDateBetween(int id, Date sDate, Date eDate, Pageable page) {
		// TODO Auto-generated method stub
		return postJpaRepository.findByGroup_groupIdAndStatus(id, sDate, eDate, true, page);
	}

	@Override
	public Page<Post> findByUserId(int id, Pageable page) {
		// TODO Auto-generated method stub
		return postJpaRepository.findByUser_userIdAndStatus(id, page, true);
	}

	@Override
	public Page<Post> findByMateId(int id, Pageable page) {
		// TODO Auto-generated method stub
		return postJpaRepository.findByMate_userIdAndStatus(id, page, true);
	}

	@Override
	public int countByUserId(int id) {
		// TODO Auto-generated method stub
		return postJpaRepository.countByUser_userIdAndStatus(id, true);
	}

	@Override
	public int countByMateId(int id) {
		// TODO Auto-generated method stub
		return postJpaRepository.countByMate_userIdAndStatus(id, true);
	}

	@Override
	public int countByGroupId(int id) {
		// TODO Auto-generated method stub
		return postJpaRepository.countByGroup_groupIdAndStatus(id, true);
	}

	@Override
	public int countByGroupIdAndCreatedDateBetween(int id, Date sDate, Date eDate) {
		// TODO Auto-generated method stub
		return postJpaRepository.countByGroup_groupIdAndCreatedDateBetweenAndStatus(id, sDate, eDate, true);
	}

	@Override
	public Post deleteById(int id) {
		Post post = postJpaRepository.findOne(id);
		postJpaRepository.updateStatus(id, false);
		return post;
		
	}

	@Override
	public Page<Post> findAllPosts(Pageable page) {
		return postJpaRepository.findByStatus(true, page);
	}
}
