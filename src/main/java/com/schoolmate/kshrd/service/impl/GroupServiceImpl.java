package com.schoolmate.kshrd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.Group;
import com.schoolmate.kshrd.repository.GroupJpaRepository;
import com.schoolmate.kshrd.service.GroupService;


@Service
public class GroupServiceImpl implements GroupService {

	@Autowired
	GroupJpaRepository groupJpaRepository;
	
	@Override
	public Group findGroupById(int id) {
		return groupJpaRepository.findOne(id);
	}

}
