package com.schoolmate.kshrd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.UserSchool;
import com.schoolmate.kshrd.repository.UserSchoolJpaRepository;
import com.schoolmate.kshrd.service.UserSchoolService;

@Service
public class UserSchoolServiceImpl implements UserSchoolService{

	@Autowired
	UserSchoolJpaRepository userSchoolJpaRepository;
	
	@Override
	public UserSchool saveUserSchool(UserSchool userSchool) {
		// TODO Auto-generated method stub
		userSchoolJpaRepository.save(userSchool);
		return userSchool;
	}

	@Override
	public void deleteByUserId(int userId) {
		userSchoolJpaRepository.deleteByUserId(userId);
	}

	
	
}
