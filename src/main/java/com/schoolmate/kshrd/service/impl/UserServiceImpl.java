package com.schoolmate.kshrd.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.UserSchool;
import com.schoolmate.kshrd.model.Group;
import com.schoolmate.kshrd.model.Notification;
import com.schoolmate.kshrd.model.School;
import com.schoolmate.kshrd.model.User;
import com.schoolmate.kshrd.model.UserGroup;
import com.schoolmate.kshrd.repository.GroupJpaRepository;
import com.schoolmate.kshrd.repository.NotificationJpaRepository;
import com.schoolmate.kshrd.repository.SchoolJpaRepository;
import com.schoolmate.kshrd.repository.UserGroupJpaRepository;
import com.schoolmate.kshrd.repository.UserJpaRepository;
import com.schoolmate.kshrd.repository.UserSchoolJpaRepository;
import com.schoolmate.kshrd.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserJpaRepository userJpaRepository;
	
	@Autowired
	NotificationJpaRepository notificationJpaRepository;
	
	@Autowired
	SchoolJpaRepository schoolJpaRepository;
	
	@Autowired
	UserSchoolJpaRepository userSchoolJpaRepository;
	
	
	@Autowired
	GroupJpaRepository groupJpaRepository;
	
	@Autowired
	UserGroupJpaRepository userGroupJpaResitory;
	
	@Override
	public User findByfacebookId(String facebookId) {
		// TODO Auto-generated method stub
		return userJpaRepository.findByFacebookId(facebookId);
	}

	@Override
	public User saveUser(User user){
		
		if(user.getUserId() == 0){
			User u = userJpaRepository.save(user);
			
			for(UserSchool us : u.getUserSchools()){
				us.setUser(u);
				userSchoolJpaRepository.save(us);
				
				School school = schoolJpaRepository.findOne(us.getSchool().getSchoolId());
				Group group = null;
				if(!groupJpaRepository.checkByNameLevelYear(school.getName(), school.getLevel(), us.getGraduatedYear())){
					 group = new Group();
					group.setName(school.getName()+","+school.getLevel()+","+us.getGraduatedYear());
					groupJpaRepository.save(group);
				}
				else {
					group = groupJpaRepository.findByName(school.getName()+","+school.getLevel()+","+us.getGraduatedYear());
				}
				
				UserGroup userGroup = new UserGroup();
				userGroup.setUser(user);
				userGroup.setGroup(group);
				userGroupJpaResitory.save(userGroup);
				
				Notification notification = new Notification();
				notification.setUser(user);
				notification.setTargetGroup(group);
				notification.setType("J");
				notification.setTargetGroupName(group.getName());
				
				notificationJpaRepository.save(notification);
			}
	
			return userJpaRepository.findOne(user.getUserId());

		}
		else{
			
			User u = userJpaRepository.findOne(user.getUserId());
			u.setFirstname(user.getFirstname() == null ? u.getFirstname() : user.getFirstname());
			u.setLastname(user.getLastname() == null ? u.getLastname() : user.getLastname());
			u.setGender(user.getGender() == null ? u.getGender() : user.getGender());
			u.setDob(user.getDob() == null ? u.getDob() : user.getDob());
			u.setLocation(user.getLocation() == null ? u.getLocation() : user.getLocation());
			u.setEmail(user.getEmail() == null ? u.getEmail() : user.getEmail());
			u.setDescription(user.getDescription() == null ? u.getDescription() : user.getDescription());
			u.setRole(user.getRole() == null ? u.getRole() : user.getRole());
			u.setProfileUrl(user.getProfileUrl() == null ? u.getProfileUrl() : user.getProfileUrl());
			u.setCoverUrl(user.getCoverUrl() == null ? u.getCoverUrl() : user.getCoverUrl());
			
			List<Integer> usIds = new ArrayList<>();
			u.getUserSchools().forEach((us)->{
				usIds.add(us.getUserSchoolId().getUser_id());
			});
			
			System.out.println("=>"+usIds);
			userJpaRepository.save(u);
			
			if(user.getUserSchools() != null){
			
				userSchoolJpaRepository.deleteByUserId(user.getUserId());
				userGroupJpaResitory.deleteByUserId(user.getUserId());
			
			for(UserSchool us : user.getUserSchools()){
				
				us.setUser(user);
				userSchoolJpaRepository.save(us);
				
				School school = schoolJpaRepository.findOne(us.getSchool().getSchoolId());
				Group group = null;
				if(!groupJpaRepository.checkByNameLevelYear(school.getName(), school.getLevel(), us.getGraduatedYear())){
					 group = new Group();
					group.setName(school.getName()+","+school.getLevel()+","+us.getGraduatedYear());
					groupJpaRepository.save(group);
				}
				else {
					group = groupJpaRepository.findByName(school.getName()+","+school.getLevel()+","+us.getGraduatedYear());
				}
				
				UserGroup userGroup = new UserGroup();
				userGroup.setUser(user);
				userGroup.setGroup(group);
				userGroupJpaResitory.save(userGroup);
				
					Notification notification = new Notification();
					notification.setUser(user);
					notification.setTargetGroup(group);
					notification.setType("J");
					notification.setTargetGroupName(group.getName());
				
				notificationJpaRepository.save(notification);
			}				
			
		}

			return null;
		}
		
	}

	@Override
	public User findById(int id) {
		// TODO Auto-generated method stub
		return userJpaRepository.findOne(id);
	}

	@Override
	public Page<User> findByUserSchoolNameAndStatus(String name, boolean status, Pageable page) {
		// TODO Auto-generated method stub
		return userJpaRepository.findByUserSchoolName(name, status, page);
	}

	@Override
	public Page<User> findMateByMateName(int userId, String name, Pageable page) {
		// TODO Auto-generated method stub
		return userJpaRepository.findMateByMateName(userId, name, true, page);
	}

	@Override
	public Page<User> findAllUser(Pageable page) {
		return userJpaRepository.findAll(page);
	}

	@Override
	public Page<User> findUserMate(int userId, Pageable page) {
		// TODO Auto-generated method stub
		return userJpaRepository.findByRemembers_User_UserId(userId, page);
	}
	
	
}
