package com.schoolmate.kshrd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.Notification;
import com.schoolmate.kshrd.repository.NotificationJpaRepository;
import com.schoolmate.kshrd.service.NotificationService;


@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	NotificationJpaRepository notificationJpaRepository;
	
	@Override
	public Notification saveNotification(Notification notification) {
		// TODO Auto-generated method stub
		return notificationJpaRepository.save(notification);
	}

	@Override
	public int countNotification() {
		// TODO Auto-generated method stub
		return notificationJpaRepository.countNotification();
	}

}
