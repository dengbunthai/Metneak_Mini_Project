package com.schoolmate.kshrd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.School;
import com.schoolmate.kshrd.repository.SchoolJpaRepository;
import com.schoolmate.kshrd.service.SchoolService;

@Service
public class SchoolServiceImpl implements  SchoolService{

	@Autowired
	SchoolJpaRepository schoolJpaRepository;
	
	@Override
	public Page<School> findByLevel(String level, Pageable page) {
		// TODO Auto-generated method stub
		return schoolJpaRepository.findByLevel(level, page);
	}

	@Override
	public Page<School> findPostDistinctByNameLike(String name, Pageable page) {
		// TODO Auto-generated method stub
		return schoolJpaRepository.findSchoolDistinctByNameLike(name+"%", page);
	}

	@Override
	public Page<String> searchSchoolByName(String schoolName, Pageable page) {
		return schoolJpaRepository.searchSchoolByName(schoolName, page);
	}

}
