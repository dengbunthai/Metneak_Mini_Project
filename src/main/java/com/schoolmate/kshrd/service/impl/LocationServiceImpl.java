package com.schoolmate.kshrd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.schoolmate.kshrd.model.Location;
import com.schoolmate.kshrd.repository.LocationJpaRepository;
import com.schoolmate.kshrd.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService{

	@Autowired
	LocationJpaRepository locationJpaRepository;
	

	@Override
	public Page<Location> findByProvinceName(String provinceName, Pageable page) {
		// TODO Auto-generated method stub
		return locationJpaRepository.findByProvinceName(provinceName, page);
	}

	@Override
	public Page<Location> findProvinceNameDistinct(Pageable page) {
		// TODO Auto-generated method stub
		return locationJpaRepository.findProvinceNameDistinct(page);
	}

}
