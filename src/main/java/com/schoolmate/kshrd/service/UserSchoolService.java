package com.schoolmate.kshrd.service;

import com.schoolmate.kshrd.model.UserSchool;

public interface UserSchoolService {
	public UserSchool saveUserSchool(UserSchool userSchool);
	public void deleteByUserId(int userId);
}
