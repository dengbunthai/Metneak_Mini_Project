package com.schoolmate.kshrd.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.schoolmate.kshrd.model.Location;

public interface LocationService {
	public Page<Location> findProvinceNameDistinct(Pageable page);
	public Page<Location> findByProvinceName(String provinceName, Pageable page);
}
