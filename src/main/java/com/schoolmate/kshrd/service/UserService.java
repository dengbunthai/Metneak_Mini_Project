package com.schoolmate.kshrd.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.schoolmate.kshrd.model.User;

public interface UserService {
	
	public Page<User> findAllUser(Pageable page);
	public User findByfacebookId(String facebookId);
	public User saveUser(User user);
	public User findById(int id);
	public Page<User> findByUserSchoolNameAndStatus(String name, boolean status, Pageable page);
	public Page<User> findUserMate(int userId, Pageable page);
	public Page<User> findMateByMateName(int userId, String name, Pageable page);
	
	
	
}